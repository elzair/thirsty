"use strict";

var expect  = require('chai').expect
  , thirsty = require(__dirname + '/../')
  ;

describe('thirsty', function() {
  describe('makePool', function() {
    it('should make a pool of 100 objects', function() {
      var obj = {name: 'Mack', age: 52};
      var pool = thirsty.makePool(obj, 100);

      expect(pool).to.be.an('object');
      expect(pool).to.have.property('initial_value');
      expect(pool.initial_value).to.equal(obj);

      expect(pool).to.have.property('array');
      expect(pool.array).to.be.an('array');
      for (let i = 0; i < pool.array.length; i++) {
        expect(pool.array[i]).to.deep.equal(obj);
      }
    });

    it('should create duplicates of original object', function() {
      var obj = {name: 'Mack', age: 52};
      var pool = thirsty.makePool(obj, 100);
      
      for (let i = 0; i < pool.array.length; i++) {
        expect(pool.array[i]).to.deep.equal(obj);
        expect(pool.array[i]).to.not.equal(obj);
      }
    });

    it('should initialize all objects as free', function() {
      var obj = {name: 'Mack', age: 52};
      var pool = thirsty.makePool(obj, 100);
      
      for (let i = 0; i < pool.array.length; i++) {
        expect(pool.free[i]).to.equal(true);
      }
    });
  });

  describe('get', function() {
    it('should return first available object', function() {
      var obj = {name: 'Mack', age: 52};
      var pool = thirsty.makePool(obj, 100);

      var dup = thirsty.get(pool);
      expect(dup).to.equal(pool.array[0]);
    });

    it('should set object as in use', function() {
      var obj = {name: 'Mack', age: 52};
      var pool = thirsty.makePool(obj, 100);

      var dup = thirsty.get(pool);
      expect(pool.free[0]).to.equal(false);
    });

    it('should return 1 after returning 0', function() {
      var obj = {name: 'Mack', age: 52};
      var pool = thirsty.makePool(obj, 100);
      var dup1 = thirsty.get(pool);

      expect(pool.current_index).to.equal(1);

      var dup2 = thirsty.get(pool);
      expect(dup2).to.equal(pool.array[1]);

      expect(pool.current_index).to.equal(2);
    });

    it('should throw an error when array is full', function() {
      var obj = {name: 'Mack', age: 52};
      var pool = thirsty.makePool(obj, 2);
      var throwsErr = false;

      thirsty.get(pool);
      thirsty.get(pool);

      try {
        thirsty.get(pool);
      }
      catch (err) {
        throwsErr = true;
        expect(err.message).to.equal('pool drained');
      }

      expect(throwsErr).to.equal(true);
    });

    it('should loop current_index around', function() {
      var obj = {name: 'Mack', age: 52};
      var pool = thirsty.makePool(obj, 2);
      thirsty.get(pool);
      thirsty.get(pool);
      
      expect(pool.current_index).to.equal(0);
    });
  });

  describe('scrub', function() {
    it('should reset the object at the specified index of the given pool', function() {
      var obj = {name: 'Mack', age: 52};
      var pool = thirsty.makePool(obj, 100);
      var index = thirsty.get(pool);
      var dup = thirsty.get(pool);
      dup.name = 'Philip';
      dup.age = 28;

      thirsty.scrub(pool, index);

      expect(pool.array[0]).to.deep.equal(obj);
    });

    it('should mark a reclaimed object as free', function() {
      var obj = {name: 'Mack', age: 52};
      var pool = thirsty.makePool(obj, 100);
      var dup = thirsty.get(pool);
      
      thirsty.scrub(pool, dup);

      expect(pool.free[0]).to.equal(true);
    });

    it('should throw an error when passed an object not in pool', function() {
      var obj = {name: 'Mack', age: 52};
      var pool = thirsty.makePool(obj, 100);
      var throwsErr = false;
      var none = {foo: 'bar'};

      try {
        thirsty.scrub(pool, none);
      }
      catch (err) {
        expect(err.message).to.equal('object not in pool');
      }
    });
  });
});
