"use strict";

const p = require('protolib');

const makePool = exports.makePool = function(obj, num) {
  const pool = {
      initial_value : obj
    , current_index : 0
    , free          : new Array(num)
    , array         : new Array(num)
  };
  
  for (let i = 0; i < num; i++) {
    pool.free[i] = true;
    pool.array[i] = p.clone(obj);
  }

  return pool;
};

const get = exports.get = function(pool) {
  let i = pool.current_index;

  do {
    if (pool.free[i]) {
      pool.current_index = (i + 1) % pool.array.length;
      pool.free[i] = false;
      return pool.array[i];
    }

    i = (i + 1) % pool.array.length;
  }
  while (i !== pool.current_index); 

  throw new Error('pool drained');
};
 
const scrub = exports.scrub = function(pool, obj) {
  for (let i = 0; i < pool.array.length; i++) {
    if (pool.array[i] === obj) {
      for (let prop in pool.array[i]) {
        if (pool.initial_value.hasOwnProperty(prop)) {
          pool.array[i][prop] = pool.initial_value[prop];
        }
        else {
          pool.array[i][prop] = undefined;
        }
      }

      pool.free[i] = true;
      return;
    }
  }

  throw new Error('object not in pool');
};

