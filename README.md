thirsty
=======

**thirsty** is a simple Object Pool implementation for Javascript.

Object pools help you tame the garbage collector by making it easy to preallocate and reuse objects. This is especially useful on mobile platforms.

For a more comprehensive introduction to Object Pools, read [Static Memory Javascript with Object Pools](http://www.html5rocks.com/en/tutorials/speed/static-mem-pools/).

Install
-------

### NPM

To install **thirsty** into a [Node.js](http://nodejs.org) project, input the following command in the root directory of your project.

    npm install thirsty --save

### Bower

To install **thirsty** into a client-side project using [Bower](http://bower.io), input the following command in the root directory of your project.

    bower install thirsty --save

Use
---

### makePool(obj, num)

Creates an Object Pool containing an array of `num` objects that are equivalent to `obj`

#### Example

This example creates a pool of 100 objects that look like `{name: 'Mack', age: 52}`.

```javascript
const thirsty = require('thirsty');

const template = {name: 'Mack', age: 52};

const pool = thirsty.makePool(template, 100);
```

### get(pool)

Returns the next available object in `pool`
Throws exception if `pool` is full

```javascript
const thirsty = require('thirsty');

const template = {name: 'Mack', age: 52};

const pool = thirsty.makePool(template, 100);

const obj = thirsty.get(pool); // Returns first object in pool
```

### scrub(pool, obj)

Resets `obj` in `pool` and marks it as free
Throws error if `obj` is not in pool 

```javascript
const thirsty = require('thirsty');

const template = {name: 'Mack', age: 52};

const pool = thirsty.makePool(obj, 2);

const obj = thirsty.get(pool);

obj.name = 'Philip';
obj.age  = 28;

thirsty.scrub(pool, obj)
// pool.array[0] is once again {name: 'Mack', age: 52}
```
